# Use cases

Site for testing: [My Pokedex](https://nagellan.github.io/Pokedex/)

Legend: ok - correct, ? - dependent on internet connection speed

## Case 1: Go to next page by pressing right-arrow button

| Test tour | 1
|---|---|
| Object to be tested | Right-arrow button must change the page from 1 to 2 |
| Test duration | n secs |
| Tester | Irek Nazmiev |
| Further testing opportunities | Anything else |

| Nr | What done | Status | Comment |
|---|---|---|---|
| 1 | Wait 10 secs | ? | Site served on GitHub Pages may load for too long |
| 2 | Check the bold number at Pager at the button of page | ok |  |
| 3 | The only bold number must be 1, left square | ok |  |
| 4 | Click the right-arrow button at the top-right corner of the site | ok |  |
| 5 | Wait 10 secs | ? | The pokemon api may respond for too long |
| 6 | Check the bold number at Pager at the button of page | ok |  |
| 7 | The only bold number must be 2, middle rectangle, left number | ok |  |

## Case 2: Change the amount of Pokemons per page to 20

| Test tour | 2
|---|---|
| Object to be tested | Button with number 20 must change the amount of shown pokemon cards to 20 from 10, underline must disappear for button with number 10, but appear for button with number 20 |
| Test duration | n secs |
| Tester | Irek Nazmiev |
| Further testing opportunities | Anything else |

| Nr | What done | Status | Comment |
|---|---|---|---|
| 1 | Wait 10 secs | ? | Site served on GitHub Pages may load for too long |
| 2 | Check the underlined number at rectangle at the top and right to Pokemon stats | ok |  |
| 3 | The only underlined number must be 10, left one | ok |  |
| 4 | Check the amount of Pokemon cards | ok |  |
| 5 | The amount of Pokemon cards must be 10 | ok |  |
| 6 | Click the number 20 at the same rectangle | ok |  |
| 7 | Wait 10 secs | ? | The pokemon api may respond for too long |
| 9 | Check the underlined number at rectangle at the top and right to Pokemon stats | ok |  |
| 10 | The only underlined number must be 20, middle one | ok |  |
| 11 | Check the amount of Pokemon cards | ok |  |
| 12 | The amount of Pokemon cards must be 20 | ok |  |

## Case 3: When Pokemon is clicked, Pokemon stats appear

| Test tour | 3
|---|---|
| Object to be tested | Clicking on Pokemon must update the Pokemon Stats aside block — Information must appear, and Pokeball icon must disappear |
| Test duration | n secs |
| Tester | Irek Nazmiev |
| Further testing opportunities | Anything else |

| Nr | What done | Status | Comment |
|---|---|---|---|
| 1 | Wait 10 secs | ? | Site served on GitHub Pages may load for too long |
| 2 | Click on the first Pokemon card, check the Pokemon's name | ok |  |
| 3 | Wait 10 secs | ? | The pokemon api may respond for too long |
| 4 | Check the Pokemon stats aside block | ok |  |
| 5 | "Pokemon stats" text must be replaces with chosen Pokemon's name | ok |  |