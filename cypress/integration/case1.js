/// <reference types="cypress" />

context("Case 1", () => {
  before(() => {
    cy.visit("https://nagellan.github.io/Pokedex/");
  });

  beforeEach(() => {
    cy.viewport(1920, 1080);
  });

  describe("changes the page from 1 to 2 by clicking on right-arrow button", () => {
    it("waits 10 secs for page load", () => {
      cy.wait(10000);
    });

    it("checks at pager that first button is active", () => {
      cy.get("#PageSelector .page.first").should("have.class", "active");
    });

    it("clicks the right-arrow button at the top-right corner of the site", () => {
      cy.get("#PageSwitcher .arrow.right").click();
    });

    it("waits 5 secs for page load", () => {
      cy.wait(5000);
    });

    it("checks at pager that second button is active", () => {
      cy.get("#PageSelector .pages-closest .page").should(
        "have.class",
        "active"
      );
    });
  });
});
